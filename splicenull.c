#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sendfile.h>
#include <errno.h>
#include <sys/syscall.h>

/* copy_file_range from: https://github.com/systemd/systemd/blob/master/src/basic/missing_syscall.h#L270 */
static inline ssize_t copy_file_range(int fd_in, loff_t *off_in,
                                      int fd_out, loff_t *off_out,
                                      size_t len,
                                      unsigned int flags) {
#  ifdef __NR_copy_file_range
        return syscall(__NR_copy_file_range, fd_in, off_in, fd_out, off_out, len, flags);
#  else
        errno = ENOSYS;
        return -1;
#  endif
}

int main() {
  ssize_t ret = 0;
  int fd = -1, sock = -1;
  
  sock = socket(AF_UNIX, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0);

  fd = open("/dev/null", O_RDONLY|O_NOCTTY|O_CLOEXEC);
  if (fd == -1)
    return 1;

  ret = write(1, "# /dev/null\n", 12);
  if (ret != 12)
    return 2;

  ret = copy_file_range(fd, NULL, 1, NULL, 0x7fffffffffffffff, 0);
  if (ret != -1 || errno != EXDEV)
    return 3;

  ret = sendfile(1, fd, NULL, 0x7fffffffffffffff);
  if (ret != -1 || errno != EINVAL)
    return 4;

  ret = splice(fd, NULL, 1, NULL, 0x7fffffffffffffff, 0);

  close (sock);
  
  return ret;
}
  
