#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[]) {
  char * today;
  size_t n = 0;
  size_t k = strlen(argv[1]);

  today = strdup(argv[1]);
  while (today) {
    today = strdup(argv[1]);
    n++;
  }

  printf ("%lu by %lu octets: %lu total\n", n, k, n*k);
  return 0;
}
