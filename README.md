Reproducer for Debian Bug #854421
=================================

The kernel seems to dump arbitrary memory when splicing from /dev/null
:(

I've reproduced this with `4.9.0-1-amd64 #1 SMP Debian 4.9.2-2 (2017-01-12) x86_64 GNU/Linux`

Please see https://bugs.debian.org/854421 for history and details.
