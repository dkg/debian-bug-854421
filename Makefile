#!/usr/bin/make -f

# wants hd from bsdmainutils

# Author: Daniel Kahn Gillmor <dkg@fifthhorseman.net>

test: splicenull
	uname -a
	for x in $$(seq 1 20); do ./splicenull | hd; done

strace: splicenull
	strace -T -tt ./splicenull | hd

%: %.c
	gcc -o $@ -pedantic -Wall -Werror $<

clean:
	rm -f splicenull rameater

.PHONY: test clean strace
